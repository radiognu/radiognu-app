/*
 * main.js -- Lógica principal de la aplicación
 *
 * Copyright 2013 - 2017 Felipe Peñailillo <breadmaker@radiognu.org>
 *                       William Cabrera   <cabrerawilliam@gmail.com>
 *
 * Este programa es software libre; puede redistribuirlo y/o modificarlo bajo
 * los términos de la Licencia Pública General GNU tal como se publica por
 * la Free Software Foundation; ya sea la versión 3 de la Licencia, o
 * (a su elección) cualquier versión posterior.
 *
 * Este programa se distribuye con la esperanza de que le sea útil, pero SIN
 * NINGUNA GARANTÍA; sin incluso la garantía implícita de MERCANTILIDAD o
 * IDONEIDAD PARA UN PROPÓSITO PARTICULAR. Vea la Licencia Pública
 * General de GNU para más detalles.
 *
 * Debería haber recibido una copia de la Licencia Pública General de GNU
 * junto con este programa; de lo contrario escriba a la Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, EE. UU.
 */

var firstTime = true;
var size = [];
var socket;
var u5a;

console.time("Evento ready disparado por el documento");
console.time("Conexión al socket");
console.time("Primeros datos desde API recibidos");

function initSocket() {
    try {
        socket = io.connect("wss://radiognu.org:7777", {
            transports: ["websocket", "htmlfile", "xhr-polling",
                "jsonp-polling"
            ]
        });
        socket.on('connect', function () {
            console.timeEnd("Conexión al socket");
        });
        socket.on('metadata', function (data) {
            if (!firstTime) {
                getInfo(data);
            } else {
                $("#init-loading").html($("<i/>").addClass(
                    "icon-spin icon-receiving").css("line-height",
                    "1.6em"));
            }
        });
        socket.on('listeners',function (listeners) {
            $("#listeners-quantity").text(listeners);
        });
        socket.on('error', function () {
            console.log("Ha ocurrido un error con la conexión.");
            statusMsg({
                message: "Error de socket",
                icon: "icon-error",
                hide: false
            });
        });
        socket.on('disconnect', function () {
            console.log("Se ha perdido la conexión con el socket.");
            statusMsg({
                message: "Desconectado",
                icon: "icon-disconnected",
                hide: false
            });
        });
        socket.on('reconnecting', function () {
            console.log("Se ha iniciado la reconexión con el socket.");
            statusMsg({
                message: "Conectando",
                icon: "icon-connecting icon-spin",
                hide: false
            });
        });
        socket.on('reconnect', function () {
            console.log("La reconexión con el socket ha sido exitosa.");
            statusMsg({
                message: "Conectado",
                icon: "icon-connected",
                hide: true
            });
        });
        socket.on('reconnect_failed', function () {
            console.log("La reconexión con el socket ha fallado.");
            statusMsg({
                message: "Desconectado",
                icon: "icon-frown",
                hide: false
            });
        });
    } catch (e) {
        $("#init").addClass("error");
        $("#init-loading").html($("<i/>").addClass("icon-error text-error")).append(
            " Falló la conexión con el socket").css("font-size", "19px");
    }
}

function initCatalog() {
    $.getJSON("https://api.radiognu.org/catalog/").done(function (data) {
        $("#catalog-wrapper").html($("<div id='catalog-main'/>"));
        /* Usando clumpy para recorrer de forma asincrona el JSON */
        var catalog_clumpy = new Clumpy();
        var catalog_length = data.length;
        var i = 0;
        catalog_clumpy.while_loop(function () {
            return i < catalog_length;
        }, function () {
            if (data[i].artists.length === 1) {
                data[i].artist = data[i].artists;
            } else {
                data[i].artist = data[i].artists.join(", ");
            }
            $("#catalog-main").append(ich.album(data[i]));
            i += 1;
        });
        /* Agregando evento a cada album para cargar sus canciones */
        $("#catalog-main").on("show.bs.collapse", ".album-songs", function () {
            var that = $(this);
            if (!that.data("loaded")) {
                that.html($("<div class='text-center'/>").html($(
                    "<i class='icon-loading icon-spin'/>")).append(
                    " Obteniendo..."));
                $.getJSON("https://api.radiognu.org/album/" + that.data(
                    "id") + "/", function (data) {
                    that.empty();
                    $.each(data, function (i, song) {
                        that.append(ich.song(song));
                    });
                    that.data({
                        loaded: true
                    });
                });
            }
        });
        var previewPlaybackInterval;
        /* Agregando evento para cargar el preview de una canción */
        $("#catalog-main").on("click", ".preview-song", function () {
            var that = $(this);
            if (!that.hasClass("hasPreview")) {
                that.button("loading");
                that.addClass("hasPreview");
                var preview_audio = $("<audio/>").attr({
                    class: "hide",
                    controls: "controls",
                    autoplay: "autoplay",
                    mozaudiochannel: "content",
                    src: "https://api.radiognu.org/preview/" +
                        that.parents(".song").data("id") +
                        ".ogg"
                }).on("play", function () {
                    var audioElement = this;
                    audioElement.volume = currentVolume;
                    $(audioElement).parents(".album-songs").find(
                        ".catalog-preview-progress").css(
                        "width", 0);
                    clearInterval(previewPlaybackInterval);
                    previewPlaybackInterval = setInterval(
                        function () {
                            $(audioElement).parents(".song")
                                .find(
                                    ".catalog-preview-progress"
                                ).css("width", audioElement
                                    .currentTime /
                                    audioElement.duration *
                                    100 + "%");
                        }, 100);
                    $("i.icon-preview-stop").parent().button(
                        "reset");
                    that.button("playing");
                    $(".u5a-html5-player audio", u5a.container)
                        .finish().animate({
                            volume: 0
                        }, {
                            queue: false,
                            duration: 500
                        });
                    $(".song-preview audio").not(this).each(function () {
                        var other_preview = $(this)[0];
                        $(other_preview).animate({
                            volume: 0
                        }, {
                            queue: false,
                            duration: 250,
                            complete: function () {
                                other_preview.pause();
                                other_preview.currentTime =
                                    0;
                                other_preview.volume =
                                    currentVolume;
                            }
                        });
                    });
                }).on("ended", function () {
                    clearInterval(previewPlaybackInterval);
                    $(this).parents(".song").find(
                        ".catalog-preview-progress").width(
                        0);
                    that.button("reset");
                    $(".u5a-html5-player audio", u5a.container)
                        .animate({
                            volume: currentVolume
                        }, {
                            queue: false,
                            duration: 500
                        });
                });
                that.after($("<span class='song-preview'/>").html(
                    preview_audio));
            } else {
                $("i.icon-preview-stop").parent().button("reset");
                var preview = that.next(".song-preview").find("audio")[
                    0];
                if (preview.paused) {
                    preview.currentTime = 0;
                    preview.volume = currentVolume;
                    preview.play();
                } else {
                    $(preview).parents(".song").find(
                        ".catalog-preview-progress").css("width", 0);
                    clearInterval(previewPlaybackInterval);
                    that.button("reset");
                    $(preview).animate({
                        volume: 0
                    }, {
                        queue: false,
                        duration: 250,
                        complete: function () {
                            preview.pause();
                            preview.currentTime = 0;
                            preview.volume = currentVolume;
                        }
                    });
                    $(".u5a-html5-player audio", u5a.container).animate({
                        volume: currentVolume
                    }, {
                        queue: false,
                        duration: 500
                    });
                }
            }
        });
        initOmnisearch(data);
    }).fail(function () {
        // Mensaje de falla de catálogo, ofrecer volver a cargar
        console.log("Mensaje de falla de catálogo, ofrecer volver a cargar");
    });
}

function initOmnisearch(data) {
    var albums = new Bloodhound({
        datumTokenizer: function (d) {
            return Bloodhound.tokenizers.whitespace(d.name);
        },
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        local: data
    });
    albums.initialize();
    $("#omnisearch-term").typeahead({
        highlight: true
    }, {
        name: "omnisearch",
        displayKey: "name",
        source: albums.ttAdapter(),
        templates: {
            header: "<h5>Álbumes</h5><hr/>",
            empty: ['<div class="empty-message text-center">',
                '<i class="icon-frown"></i> No hay resultados', '</div>'
            ].join('\n'),
            suggestion: Handlebars.compile('<div>{{name}} – {{year}}</div>')
        }
    }).on("typeahead:selected", function (ev, data) {
        var element = $(".album-info[data-target='#album-" + data.id + "']");
        $('#catalog-wrapper').animate({
            scrollTop: element.offset().top - $('#catalog-wrapper').offset()
                .top + $('#catalog-wrapper').scrollTop()
        }, 500);
        $(this).blur();
        if (element.hasClass("collapsed")) element.click();
    }).on("input", function () {
        if ($(this).val() !== "" && $("#omnisearch .icon-search").hasClass("in")) {
            $("#omnisearch .icon-backspace").addClass("in");
            $("#omnisearch .icon-search").removeClass("in");
            $("#omnisearch-backspace-click-space").removeClass("hide");
        } else if ($(this).val() === "" && $("#omnisearch .icon-backspace").hasClass(
                "in")) {
            $("#omnisearch .icon-backspace").removeClass("in");
            $("#omnisearch .icon-search").addClass("in");
            $("#omnisearch-backspace-click-space").addClass("hide");
        }
    });
    $("#omnisearch-backspace-click-space").click(function () {
        if ($("#omnisearch-term").val() !== "") {
            $("#omnisearch")[0].reset();
            $("#omnisearch-term").trigger("input");
        }
    });
}

var sources = ["https://audio.radiognu.org/radiognuam.ogg",
    "https://audio.radiognu.org/radiognu3.ogg",
    "https://audio.radiognu.org/radiognu2.ogg",
    "https://audio.radiognu.org/radiognu.ogg"
];

function initOptions() {
    $("#cover-quality-option").change(function () {
        var value = $(this).val();
        if (Modernizr.cssfilters) {
            if (value === "1" || value === "0") {
                $("#config-cover-blur").collapse("show");
            } else {
                $("#config-cover-blur").collapse("hide");
            }
        }
        localStorage.setItem("cover_quality", value);
    });
    if (localStorage.getItem("cover_quality") !== null) {
        $("#cover-quality-option").val(localStorage.getItem("cover_quality")).change();
    } else {
        localStorage.setItem("cover_quality", $("#cover-quality-option").val());
    }
    if (Modernizr.cssfilters) {
        $("#cover-blur-yes").click(function () {
            if ($(this).hasClass("btn-default")) {
                $("#cover-blur-yes").removeClass("btn-default").addClass(
                    "btn-success");
                $("#cover-blur-no").removeClass("btn-danger").addClass(
                    "btn-default");
                localStorage.setItem("cover_blur", "yes");
                $("#cover").addClass('blurred');
            }
        });
        $("#cover-blur-no").click(function () {
            if ($(this).hasClass("btn-default")) {
                $("#cover-blur-yes").removeClass("btn-success").addClass(
                    "btn-default");
                $("#cover-blur-no").removeClass("btn-default").addClass(
                    "btn-danger");
                localStorage.setItem("cover_blur", "no");
                $("#cover").removeClass('blurred');
            }
        });
        if (localStorage.getItem("cover_blur") !== null) {
            $("#cover-blur-" + localStorage.getItem("cover_blur")).click();
            $("#cover").toggleClass('blurred', localStorage.getItem("cover_blur") ===
                "yes");
        } else {
            localStorage.setItem("cover_blur", "no");
            $("#cover").removeClass('blurred');
        }
    }
    if (localStorage.getItem("player_quality") !== null) {
        $("#player-quality-option").val(localStorage.getItem("player_quality"));
        source = u5a.source = sources[localStorage.getItem("player_quality")];
        $(".u5a-html5-player audio", u5a.container).attr("src", sources[localStorage.getItem(
            "player_quality")]);
    }
    $("#player-quality-option").change(function () {
        localStorage.setItem("player_quality", $(this).val());
        u5a.source = sources[$(this).val()];
        u5a.resetPlayer();
    });
    // Verifica por soporte de notificaciones (y soporta la API antigua de
    // Gecko, mozNotification)
    if (!!window.Notification || !!navigator.mozNotification) {
        // Error conocido: Al activamente denegar la autorización para permitir
        // notificaciones, no hay forma (conocida) de volver a preguntar por
        // autorización, a menos que el usuario cambie la configuración de
        // permisos en su navegador.
        $("#notification-option-yes").click(function () {
            if ($(this).hasClass("btn-default")) {
                if ("mozNotification" in navigator) {
                    localStorage.setItem("notifications", "yes");
                } else if ("Notification" in window) {
                    if (Notification.permission !== 'granted') {
                        Notification.requestPermission(function (result) {
                            if (result === 'denied' || result ===
                                'default') {
                                $("#notification-option-no").click();
                                localStorage.setItem("notifications",
                                    "no");
                            } else {
                                localStorage.setItem("notifications",
                                    "yes");
                                $("#config-notification-format").collapse(
                                    "show");
                            }
                        });
                    } else {
                        localStorage.setItem("notifications", "yes");
                        $("#config-notification-format").collapse("show");
                    }
                }
            }
            $("#notification-option-yes").removeClass("btn-default").addClass(
                "btn-success");
            $("#notification-option-no").removeClass("btn-danger").addClass(
                "btn-default");
        });
        $("#notification-option-no").click(function () {
            if ($(this).hasClass("btn-default")) {
                $("#notification-option-yes").removeClass("btn-success").addClass(
                    "btn-default");
                $("#notification-option-no").removeClass("btn-default").addClass(
                    "btn-danger");
                localStorage.setItem("notifications", "no");
                $("#config-notification-format").collapse("hide");
            }
        });
        $("#config-notification").removeClass("hide");
        if (localStorage.getItem("notifications") !== null) {
            $("#notification-option-" + localStorage.getItem("notifications")).click();
        }
        $("#notification-format-option-yes").click(function () {
            if ($(this).hasClass("btn-default")) {
                $("#notification-format-option-yes").removeClass("btn-default")
                    .addClass("btn-success");
                $("#notification-format-option-no").removeClass("btn-danger").addClass(
                    "btn-default");
                localStorage.setItem("notifications-format", "yes");
            }
        });
        $("#notification-format-option-no").click(function () {
            if ($(this).hasClass("btn-default")) {
                $("#notification-format-option-yes").removeClass("btn-success")
                    .addClass("btn-default");
                $("#notification-format-option-no").removeClass("btn-default").addClass(
                    "btn-danger");
                localStorage.setItem("notifications-format", "no");
            }
        });
        if (localStorage.getItem("notifications-format") !== null) {
            $("#notification-format-option-" + localStorage.getItem(
                "notifications-format")).click();
        }
    }
}

var statusHideTimeout;

function statusMsg(config) {
    clearTimeout(statusHideTimeout);
    if ($("#status i").hasClass(config.icon)) {
        $("#status").addClass("in").find("#status-message").html(" " + config.message);
    } else {
        $("#status").addClass("in").html($("<i/>").addClass(config.icon)).append($(
            "<span id='status-message'/>").html(" " + config.message));
    }
    if (config.hide) {
        statusHideTimeout = setTimeout(function () {
            $("#status").removeClass("in");
        }, 5000);
    }
}

//~ function reconnectSocket(){
//~ if(socket!=undefined){
//~ }
//~ }

function htmlEntities(str) {
    return String(str).replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g,
        '&gt;').replace(/"/g, '&quot;');
}

var coverPreview, coverImg, currentMetadata = {
    artist: "",
    title: ""
};

function fetchInfo(metadata) {
    if (!firstTime) $(".progress").addClass("progress-striped active");
    $.ajax({
        dataType: "json",
        url: "https://api.radiognu.org/?no_cover",
        data: metadata
    }).done(function (data) {
        if ($("#init").length == 1) $("#init").removeClass("in").on("bsTransitionEnd",
            function () {
                $(this).remove();
            });
        if (firstTime) {
            currentMetadata.artist = data.artist;
            currentMetadata.title = data.title;
            if (localStorage.getItem('cover_quality') >= 2) {
                coverPreview = new Image();
                coverPreview.onload = function () {
                    $("#cover-preview").css("background-image", "url(" +
                        coverPreview.src + ")").addClass("in");
                };
                coverPreview.src = "https://api.radiognu.org/cover/" + data.album_id +
                    ".png?img_size=50";
            }
            coverImg = new Image();
            coverImg.onload = function () {
                $("#cover").css("background-image", 'url(' + coverImg.src +
                    ')').addClass("in");
                $("#cover-preview-loader").addClass("hide");
            };
            coverImg.src = "https://api.radiognu.org/cover/" + data.album_id +
                ".png?img_size=" + ((localStorage.getItem("cover_quality") !==
                        null) ? size[localStorage.getItem("cover_quality")] :
                    size[3]);
            $("#cover").addClass('quality-' + localStorage.getItem(
                'cover_quality')).toggleClass('blurred', localStorage.getItem(
                "cover_blur") === "yes");
            $("#title").html(data.title);
            $("#artist").html(data.artist + ' (' + data.country + ')');
            $("#album").text(data.album + ' (' + data.year + ')');
            $("#license").text(data.license.shortname);
            $("#listeners-quantity").text(data.listeners);
            if (data.isLive) {
                $("#live").removeClass("hidden").addClass("in");
            }
            document.title = [data.artist, data.title].join(" - ");
            initSocket();
            firstTime = false;
        } else {
            if (localStorage.getItem("notifications") === "yes") {
                if ("mozNotification" in navigator) {
                    // Error conocido: La API antigua no tiene ni opción tag,
                    // ni método close(), lo que hace que la lista de
                    // notificaciones se llene rápidamente
                    navigator.mozNotification.createNotification(data.title,
                        data.artist + " (" + data.country + ")\n" + data.album +
                        " (" + data.year + ")" + (data.license.shortname !==
                            undefined ? "\n" + data.license.shortname : ""),
                        "https://api.radiognu.org/cover/" + data.album_id +
                        ".png?img_size=50").show();
                } else if (Notification.permission === 'granted') {
                    var notifBody = data.artist + " (" + data.country + ")\n" +
                        data.album + " (" + data.year + ")" + (data.license.shortname !==
                            undefined ? "\n" + data.license.shortname : "");
                    var n = new Notification(data.title, {
                        body: localStorage.getItem(
                                "notifications-format") === "yes" ?
                            htmlEntities(notifBody) : notifBody,
                        icon: "https://api.radiognu.org/cover/" + data.album_id +
                            ".png?img_size=50",
                        tag: "radiognu"
                    });
                    setTimeout(function () {
                        n.close();
                    }, 7000);
                }
            }
            $('#player-info').collapse("hide").one("hidden.bs.collapse",
                function () {
                    $("#title").html(data.title);
                    $("#artist").html(data.artist + ' (' + data.country +
                        ')');
                    $("#album").text(data.album + ' (' + data.year + ')');
                    $("#license").text((data.license.shortname !==
                        undefined) ? data.license.shortname : "");
                    $("#listeners-quantity").text(data.listeners);
                    if (data.isLive) {
                        $("#live").removeClass("hidden");
                        setTimeout(function () {
                            $("#live").addClass("in");
                        }, 10);
                    } else {
                        $("#live").one("bsTransitionEnd", function () {
                            $("#live").addClass("hidden");
                        }).removeClass("in");
                    }
                    document.title = [data.artist, data.title].join(" - ");
                    $(this).collapse("show");
                });
            if (localStorage.getItem('cover_quality') >= 2) {
                coverPreview.onload = null;
                if ($("#cover-preview").hasClass("in")) {
                    $("#cover-preview").removeClass("in").one("bsTransitionEnd",
                        function () {
                            $(this).off("bsTransitionEnd").css("background-image",
                                "");
                            coverPreview = new Image();
                            coverPreview.onload = function () {
                                $("#cover-preview").css("background-image",
                                    "url(" + coverPreview.src + ")").addClass(
                                    "in");
                            };
                            coverPreview.src = "https://api.radiognu.org/cover/" +
                                data.album_id + ".png?img_size=50";
                        });
                } else {
                    $("#cover-preview").css("background-image", "").css(
                        "background-image", "url('https://api.radiognu.org/cover/" +
                        data.album_id + ".png?img_size=50')").addClass("in");
                }
            } else {
                $("#cover-preview.fade.in").removeClass("in").css(
                    "background-image", "");
            }
            $('#cover').removeClass("in").one("bsTransitionEnd", function () {
                $("#cover-preview-loader").removeClass("hide");
                $(this).off("bsTransitionEnd").css('background-image', '');
                coverImg.onload = null;
                coverImg = new Image();
                coverImg.onload = function () {
                    $("#cover").css("background-image", 'url(' + coverImg.src +
                        ')').addClass("in");
                    $("#cover-preview-loader").addClass("hide");
                };
                coverImg.src = "https://api.radiognu.org/cover/" + data.album_id +
                    ".png?img_size=" + ((localStorage.getItem("cover_quality") !==
                            null) ? size[localStorage.getItem("cover_quality")] :
                        size[3]);
                $(this).removeClass(function (index, css) {
                    return (css.match(/\bquality-\S+/g) || []).join(' ');
                }).toggleClass('quality-' + localStorage.getItem(
                        'cover_quality'), localStorage.getItem("cover_blur") ===
                    "yes");
                // Corrige un error de visualizacion cuando se actualiza la
                // informacion de la seccion #player-page y esta no se
                // encuentra visible.
            }).filter(":not(:visible)").css("background-image",
                'url("https://api.radiognu.org/cover/' + data.album_id +
                '.png?img_size=' + ((localStorage.getItem("cover_quality") !== null) ?
                    size[localStorage.getItem("cover_quality")] : size[3]) + '")').removeClass(
                function (index, css) {
                    return (css.match(/\bquality-\S+/g) || []).join(' ');
                }).addClass('quality-' + localStorage.getItem('cover_quality'),
                localStorage.getItem("cover_blur") === "yes");
            $(".progress").removeClass("progress-striped active");
        }
        if (metadata === undefined) {
            console.timeEnd("Primeros datos desde API recibidos");
        }
    }).fail(function () {
        if (firstTime) {
            $("#init").addClass("error");
            $("#init-loading").html(
                '<div>' + '<i class="icon-error"></i>' +
                ' Falló la conexión con la API' + '</div>' +
                '<button class="btn btn-primary" ' +
                'onclick="$(this).button(\'loading\');fetchInfo();" ' +
                'data-loading-text="<i class=\'icon-reload icon-spin\'></i> Reintentando...">' +
                '<i class="icon-reload"></i>' + ' Reintentar</button>');
        } else {
            setTimeout(function () {
                console.log(
                    "Falló la conexión con la API. Reintentando..."
                );
                fetchInfo(metadata);
            }, 1000);
        }
    });
}

function getInfo(metadata) {
    if (metadata === undefined) {
        fetchInfo();
    } else if (currentMetadata.title != metadata.title && currentMetadata.artist !=
        metadata.artist) {
        currentMetadata = metadata;
        console.log("Recibidos nuevos metadatos: " + JSON.stringify(metadata));
        fetchInfo(metadata);
    } else {
        console.log("Recibidos metadatos duplicados: " + JSON.stringify(metadata));
    }
}

function getWindowMinSize() {
    size[3] = ($(window).outerWidth(true) < $(window).outerHeight(true)) ?
        $(window).outerWidth(true) : $(window).outerHeight(true);
    size[2] = Math.round(size[3] / 2);
    size[1] = Math.round(size[3] / 4);
    size[0] = Math.round(size[3] / 8);
}

var currentVolume = 1;

$(document).ready(function () {
    /* Cargando en CSS de forma asincrona */
    $("head").append($("<link/>").attr({
        rel: "stylesheet",
        href: "css/main.css"
    }));
    if (!Modernizr.localstorage) {
        $('#init').addClass('warning');
        $("#init-loading").html($("<i/>").addClass("icon-error")).append(
            " Debe permitir el uso de localStorage y appCache").css(
            "font-size", "19px");
    }
    console.timeEnd("Evento ready disparado por el documento");
    u5a = new UniversalHTML5Audio({
        selector: "radiognu",
        className: "",
        template: "player-template",
        src: "https://audio.radiognu.org/radiognu2.ogg",
        events: {
            error: function () {
                statusMsg({
                    message: "Error de reproducción",
                    icon: "icon-error",
                    hide: true
                });
            },
            playing: function () {
                statusMsg({
                    message: "Reproduciendo",
                    icon: "icon-play",
                    hide: true
                });
            },
            loadstart: function () {
                $("#status").removeClass("in");
            },
            waiting: function () {
                statusMsg({
                    message: "Cargando buffer",
                    icon: "icon-loading icon-spin",
                    hide: false
                });
            }
        }
    });
    /* Inicializando noUISlider */
    noUiSlider.create(document.getElementById('u5a-volume-slider'), {
        range: {
            'min': 0,
            'max': 1
        },
        start: 100,
        handles: 1
    }).on("slide", function (strVals, zero, numVals) {
        u5a.audio.volume = currentVolume = numVals[0];
    });
    /* Inicializando plantillas */
    var templates = [{
        "name": "album",
        "template": "<div data-target='#album-{{id}}' data-toggle='collapse'" +
            " class='album-info collapsed'>{{name}} de {{artist}}</div><div" +
            " id='album-{{id}}' data-id='{{id}}'class='album-songs" +
            " collapse'></div"
    }, {
        "name": "song",
        "template": "<div id='song-{{id}}' data-id='{{id}}' " +
            "class='song'><span class='catalog-preview-progress'></span>" +
            "<span class='catalog-song-title'>{{name}}</span>" +
            "<span class='btn-group pull-right'>" +
            "<button data-loading-text='<i class=\"icon-loading icon-spin\">" +
            "</i>' data-playing-text='<i class=\"icon-preview-stop\"></i>' " +
            "class='btn btn-primary preview-song'><i " +
            "class='icon-preview-play'></i></button></span></div>"
    }];
    $.each(templates, function (i, template) {
        ich.addTemplate(template.name, template.template);
    });
    initCatalog();
    initOptions();
    getWindowMinSize();
    $("#player-info").collapse("show");
    getInfo();
    /* Inicializa botón para mostrar el software de terceros */
    $("#showThirdPartySoftware").click(function () {
        $(this).addClass('hide');
    });
    /* Corrige un problema de visualización al mostrar la sección software de
       terceros, refs #20 */
    $("#license-info").on("shown.bs.collapse", function () {
        $(window).resize();
    });
    /* Define mostrar nuevamente el boton "Mostrar" en la seccion software de
       terceros */
    $("#about-modal").on("hidden.bs.modal", function () {
        $(this).find(".hide").removeClass("hide");
        $("#license-info").removeClass("in");
    });
});

var updateTimeout = false,
    scheduleUpdate = function (delay) {
        clearTimeout(updateTimeout);
        updateTimeout = setTimeout(function () {
            try {
                window.applicationCache.update();
            } catch (ex) {
                console.log('appCache.update: ' + ex);
            }
        }, delay || 300000);
    };

scheduleUpdate(3000);

$(window).load(function () {
    // Verifica si hay un nuevo appCache disponible
    window.applicationCache.addEventListener('updateready', function (e) {
        if (window.applicationCache.status == window.applicationCache.UPDATEREADY) {
            $("#about-modal").modal("hide");
            $("#new-version-modal").modal("show");
            scheduleUpdate();
        }
    }, false);
    window.applicationCache.addEventListener('noupdate', function () {
        scheduleUpdate();
    }, false);
    window.applicationCache.addEventListener('error', function () {
        scheduleUpdate();
    }, false);
}).resize(function () {
    getWindowMinSize();
}).focus(function () {
    $("#cover").trigger("transitionend").addClass("in");
    if (!$("#live").hasClass("in")) {
        $("#live").off("bsTransitionEnd").addClass("hidden");
    }
});
